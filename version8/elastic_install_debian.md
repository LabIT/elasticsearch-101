# Installation and Configuration of ElasticSearch
# https://gitlab.com/LabIT/elasticsearch.git
# Pre-requisites 

## Update System

    sudo apt-get update

## Install wget if it is not on the system

    sudo apt-get install wget -y

# Manual ElK Stack Installation steps

## 1. Download and install public signing key 

    wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo apt-key add -

## 2. Install apt-transport-https package

    sudo apt-get install apt-transport-https -y

## 3. Save directory definitions

    echo "deb https://artifacts.elastic.co/packages/8.x/apt stable main" | sudo tee -a /etc/apt/sources.list.d/elastic-8.x.list


## 4. Update and Install elasticsearch

    sudo apt-get update && sudo apt-get install elasticsearch && sudo apt-get install logstash && sudo apt-get install kibana

    ### Note down the elastic superuser password from here

## 5. configure elasticsearch

    sudo su
    nano /etc/elasticsearch/elasticsearch.yml

    change cluster name
    cluster.name: demo-elk  

    give the cluster a descriptive name
    node.name: elk-1 

    change network binding
    network.host: 0.0.0.0  

    setup discovery.type as single node
    discovery.type: single-node

## 6. Start Elasticsearch service

    sudo systemctl start elasticsearch

## 7. validate Elasticsearch cluster health

    curl -k -u elastic:<password> https://localhost:9200/_cluster/health?pretty   ## -k to ignore ssl verification 


## 8. Rest password for kibana_system user 
/usr/share/elasticsearch/bin/elasticsearch-reset-password -u kibana_system

## 8. configure kibana
    
    nano /etc/kibana/kibana.yml

    uncomment server.port
    server.port: 5601

    server base url however this needs to be corrected everytime you start and stop the server
    server.publicBaseUrl: "http://192.168.1.3:5601/"

    change server.host
    server.host: "0.0.0.0"
    
    change server.name
    server.name: "demo-kibana"
    
    uncomment elasticsearch.host
    elasticsearch.hosts: ["https://localhost:9200"]

    add kibana_system user credentials

changed ssl verification mode to none for self-signed certificates

elasticsearch.ssl.verificationMode: none

    
## 9. start Kibana service

    systemctl start kibana
    
## 10. enable elasticsearch and kibana

    systemctl enable elasticsearch
    systemctl enable kibana
    
## Miscellaneous
 
a. To list content of elasticsearch keystore 

/usr/share/elasticsearch/bin/elasticsearch-keystore list

b. To view value of a key
/usr/share/elasticsearch/bin/elasticsearch-keystore show xpack.security.http.ssl.keystore.secure_password

c. To open a .p12 file

openssl pkcs12 -nokeys -info -in /etc/elasticsearch/certs/http.p12


Zm8BJNDWHaGm9o2AihkH
O4NzrJlZUDF0G7lk+APi

curl -k -u elastic:b4lPp01od4fN0k4KMsZC https://192.168.1.28:9200/_cluster/health?pretty