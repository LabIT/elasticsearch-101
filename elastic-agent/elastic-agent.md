
# Elastic Agent and Fleet Installation

## 1.  Add the following two settings to your elasticsearch.yml
    xpack.security.enabled: true
    xpack.security.authc.api_key.enabled: true


## Fleet server 

## 1. Download Elastic Agent on Fleet server 

    curl -L -O https://artifacts.elastic.co/downloads/beats/elastic-agent/elastic-agent-8.0.0-linux-x86_64.tar.gz

    tar -xzf elastic-agent-8.0.0-linux-x86_64.tar.gz

## 2. Login to Kibana and go to Fleet -> Settings

## 3. edit output

### change elasticsearch host url

    https://<private_ip_of_es_server>:9200

## 3. Go to ssh terminal of elk server 

    cat /etc/elasticsearch/certs/http_ca.crt

### Copy the certificate value from BEGIN CERTIFICATE to END CERTIFICATE


## 4. Update fleet -> settings -> Output -> Advanced YAML

     ssl:
     certificate_authorities:
     - |
       <Paste_es_ca_cert>

### Example 

     ssl:
     certificate_authorities:
     - |
        -----BEGIN CERTIFICATE-----
        MIIFWTCCA0GgAwIBAgIUNdfypX7B9Q+Fs9q9/UgKSAiN5pAwDQYJKoZIhvcNAQEL
        BQAwPDE6MDgGA1UEAxMxRWxhc3RpY3NlYXJjaCBzZWN1cml0eSBhdXRvLWNvbmZp
        Z3VyYXRpb24gSFRUUCBDQTAeFw0yMjAyMjcxNzIzMDFaFw0yNTAyMjYxNzIzMDFa
        MDwxOjA4BgNVBAMTMUVsYXN0aWNzZWFyY2ggc2VjdXJpdHkgYXV0by1jb25maWd1
        cmF0aW9uIEhUVFAgQ0EwggIiMA0GCSqGSIb3DQEBAQUAA4ICDwAwggIKAoICAQCY
        HK+4iQa4D9XbojidnaTXZmgveuvSd1WwlxUqHJH98VXRY+Jm9IBthlNUJ5Ck0K2N
        pa15wlZUgXJ0GrNtNfh8qtTDZyOI76A38at/c7UrD1rk0AC1hIxokiM+YUHb5D4T
        M3jli1jZmffZwNDpc6w1AAXTH6WsgeIrgT5hnrJzFCOUzgA3izB22CEXRFf+F4C7
        9QpDLaXH3yE3ju2AI1qMPUD7mt13mERFa5eZTI/ktnVwFHMcnpM4XRAzhPR0U4fT
        Aj6WhjnkD/GN7a2eSpAttrXl7a02wnX3SSMzzYgi1H44txUFuPgNTGiEckRG8MsH
        Snn8XOW3nAc5iSLLgeAAyuMz44zIPYuZYQxsjijiwswDHXzwVs3GdW5aFC0vCm3D
        o+dEgMe7AyMSiVvEx7phwNd12ZmcegBE9bg9Q1EtMaL9H9iJqL+4Kg1QA09Hb7jl
        h3gFizHA0W4AYg7OWjGuHmKXrJjXvtM4XnmglBVpER6SEhVPrjJgOrwjAFAzUfDp
        EhSZJRcGqPv4R0m/4Vn9SXNBXpaiwwphEA6g+g4u8WIkApLUt+BuoC9mD2Jh4DNb
        QqPdYDC/G0jNTyiIu6AgrJPjMoZ/NQ1N2VrppiRr2ImpkoonUVYVS50UAws7SKoL
        6K9sA28uxV+PR8bPYX8OnuM4TOY99ZxgVeSb0taalQIDAQABo1MwUTAdBgNVHQ4E
        FgQUbTtZdU+MAVbHU9jH9rlE/46wLIYwHwYDVR0jBBgwFoAUbTtZdU+MAVbHU9jH
        9rlE/46wLIYwDwYDVR0TAQH/BAUwAwEB/zANBgkqhkiG9w0BAQsFAAOCAgEAGsvN
        fhgIbjujSX21zOx6OtMP/g5RHBXBhL0cPz2E20GC3Xk/8h0M+jt1h+ZzRK5zykHO
        06C7CVUEwR0a1td0/T7Lzem08Tv/oFTXVPahlhxncTWDolxQKthrXUuAaed0AnHs
        BIzpNv99QF8I4ChjB3ZVZe+qzRT3NjDIqKuKc2ZE9KuTACo1T8Yq/Ddjd0q/VkiO
        pqo//kmIltFKiKP/LW649UDpSe1gqvJKNECjyH/fHmj68Hsn73+1v2UsPXuwnWiK
        pnwjnCTOQTUxKy7LOmKr5tymJQ+r5icLJumAFq1aoia6aatl9Ef36F0DH7B2u6j+
        AWCPvQiCdw7VD8viBowfmv14xU7xhH7arJJ16byAtIBhw2G1zCLgo9hG4Xo2Ehcq
        COikoGo4mrq/4o/KwpnDBl5RFuA2+jmrBsMqBNTE6kBqdErz1UHKc1+haLEj2FW6
        m0Y8ThorkJPOKUqUGG+C5aqIhErQt6/pGrgftEKleYcudITj5ksDUl5feU/97+VX
        ccwdV0x8hK77NSLIbHWjLjURiqKtJtWyr1YXjYvhmT/xSszTZMIm5sFBRVRnZ9w5
        2yFNNXTioB8Txnmj2PGizzR5jlQ/z05tvwWgoy1pFlXX6AfuabhK3XC0XEXm4lwh
        9N70MgACAyRmpjrEg7VeSoDyMMzxfYZ1oS6/cns=
        -----END CERTIFICATE-----

### Save and deploy

## 5. Go to Fleet->Agents 
###       - Add Fleet server host
###       - Generate service token

## 6. Copy the Fleet server deployment command


## 7. Install elastic-agent [Update the command with below settings]

  
sudo ./elastic-agent install -f \   
  --url=https://192.168.1.32:8220 \                       
  --fleet-server-es=https://192.168.1.31:9200  \    --fleet-server-service-token=AAEAAWVsYXN0aWMvZmxlZXQtc2VydmVyL3Rva2VuLTE2NDU5ODQ4NTEwNTc6MUpidGZtTFpRWEN3cEpZZFFnR21qdw \           
  --fleet-server-policy=499b5aa7-d214-5b5d-838b-3cd76469844e \
  --fleet-server-es-insecure \
  --insecure 

sudo ./elastic-agent install  \
  --fleet-server-es=https://192.168.1.31:9200 \
  --fleet-server-service-token=AAEAAWVsYXN0aWMvZmxlZXQtc2VydmVyL3Rva2VuLTE2NDU5ODQ4NTEwNTc6MUpidGZtTFpRWEN3cEpZZFFnR21qdw \
  --fleet-server-policy=499b5aa7-d214-5b5d-838b-3cd76469844e \
  --fleet-server-insecure-http

## 8. check status 

sudo systemctl status elastic-agent

### By default elastic-agent logs to stdout 

## 9. Ensure fleet-server is in "healthy" status and logs show up under logs tab

## Install agent on webserver

## 1.  Download elastic agent 

curl -L -O https://artifacts.elastic.co/downloads/beats/elastic-agent/elastic-agent-8.0.0-linux-x86_64.tar.gz

tar -xzf elastic-agent-8.0.0-linux-x86_64.tar.gz

##2. Under Fleet -> agent -> click on add agent 

##3. Install and configure elastic-agent 


## Copy the command from elastic-agent UI and add --insecure to update for self-signed certificates 

sudo ./elastic-agent install --url=https://192.168.1.10:8220 --enrollment-token=<your_token>== --insecure

## configure agent policy 

## Add an integration for apache

## Assign this policy to agent 

## Check apach dashboard on KIbana
## Check metric




