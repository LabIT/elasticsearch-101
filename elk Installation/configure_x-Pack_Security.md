# Configure X-Pack Security 

## 1. stop kibana

    sudo systemctl stop kibana

## 2. Stop elasticsearch 

    sudo systemctl stop elasticsearch

## 3. enable xpack in elasticsearch.yml

    sudo nano /etc/elasticsearch/elasticsearch.yml
    xpack.security.enabled: true
    xpack.security.http.ssl.enabled: false     # needed for elasticsearch 8.x to run on http
    xpack.security.transport.ssl.enabled: false   # needed for elasticsearch 8.x to run without ssl

## 4. Setup default user passwords

    sudo systemctl start elasticsearch
    cd /usr/share/elasticsearch/bin
    sudo ./elasticsearch-setup-passwords auto


## System Passwwords 

### << Default user passwords go here >>


Changed password for user apm_system
PASSWORD apm_system = E8GAtoCQvRfvivKMhQ03

Changed password for user kibana_system
PASSWORD kibana_system = hkGtwoxgUJoA4A3rRkIM

Changed password for user kibana
PASSWORD kibana = hkGtwoxgUJoA4A3rRkIM

Changed password for user logstash_system
PASSWORD logstash_system = STT1zOkUZPazVMZYFXRA

Changed password for user beats_system
PASSWORD beats_system = MfkQcXE7YnLMAQEIUgVw

Changed password for user remote_monitoring_user
PASSWORD remote_monitoring_user = FyNE2jZqulTavxNGMWvz

Changed password for user elastic
PASSWORD elastic = P38yIQJTLBJiWQmhuigv

## 5. Add the default username in kibana
elasticsearch.username: "kibana"

elasticsearch.password: "new_password"



