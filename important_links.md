# Elasticsearch Important reading links 

## Elasticsearch blog
https://www.elastic.co/blog/found-elasticsearch-from-the-bottom-up

## Installation links

### Debian Installation : https://www.elastic.co/guide/en/elasticsearch/reference/current/deb.html
### RPM Installation : https://www.elastic.co/guide/en/elasticsearch/reference/current/rpm.html
### Windows Installation : https://www.elastic.co/guide/en/elasticsearch/reference/current/zip-windows.html

## Datasets :
https://www.kaggle.com/datasets

## Kibana
### Dashboards and visualisations : https://www.elastic.co/guide/en/kibana/current/dashboard.html
### Kibana Canvas : https://www.elastic.co/guide/en/kibana/current/canvas.html
### Authentication realms : 
#### 1. https://www.elastic.co/guide/en/elasticsearch/reference/current/realms.html
#### 2. https://www.elastic.co/blog/a-deep-dive-into-elasticsearch-authentication-realms
#### 3. https://www.elastic.co/guide/en/kibana/current/kibana-authentication.html
### Kibana spaces : https://www.elastic.co/guide/en/kibana/master/xpack-spaces.html

## Logstash : https://www.elastic.co/guide/en/logstash/current/getting-started-with-logstash.html

## Beats 
### Metricbeat : 
#### 1. Overview : https://www.elastic.co/guide/en/beats/metricbeat/current/metricbeat-overview.html
#### 2. How metricbeat works : https://www.elastic.co/guide/en/beats/metricbeat/current/how-metricbeat-works.html
### Filebeat : https://www.elastic.co/guide/en/beats/filebeat/current/filebeat-overview.html
### Auditbeat : https://www.elastic.co/guide/en/beats/auditbeat/current/auditbeat-overview.html
### Heartbeat : https://www.elastic.co/guide/en/beats/heartbeat/current/heartbeat-overview.html
### Packetbeat : https://www.elastic.co/guide/en/beats/packetbeat/current/packetbeat-overview.html

## Cluster
### Overview of cluster nodes : https://www.elastic.co/guide/en/elasticsearch/reference/current/modules-node.html
### Adding and removing cluster nodes : https://www.elastic.co/guide/en/elasticsearch/reference/current/add-elasticsearch-nodes.html
### elasticsearch scalability : https://www.elastic.co/guide/en/elasticsearch/reference/current/scalability.html

## Elasticsearch 8.x 
### Whats new : https://www.elastic.co/blog/whats-new-elastic-8-0-0
### release highlights : https://www.elastic.co/guide/en/elasticsearch/reference/8.0/release-highlights.html
### release notes : https://www.elastic.co/guide/en/elasticsearch/reference/8.0/es-release-notes.html

## Fleet and elastic agent : 
### Overview : https://www.elastic.co/guide/en/fleet/current/fleet-overview.html